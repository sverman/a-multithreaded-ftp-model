#include "srv_utils.h"
#include "unpifiplus.h"
#include <setjmp.h>
#include <sys/time.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <netinet/in.h>

void serv_read_input(char* filename, serv_input* in){
  
  FILE* fp = fopen(filename, "r");
  if(fp==NULL){
    printf("Error opening input file %s\n", filename);
    exit(-1);
  }
  
  int n=0;
  if((n=fscanf(fp, "%d\n%d", &(in->port), &(in->send_window_size)))!=2){
    printf("Error reading inputs from %s (%d out of 2 read)\n", filename, n);
    exit(-1);
  }
  fclose(fp);
  
  printf("  Well-known port number: %d\n", in->port);
  printf("  Sending sliding window size: %d\n\n", in->send_window_size);
  
  return;
}

int init_dgram(datagram* dgram){
  memset((struct header*)(&(dgram->hdr)), 0, sizeof(header));
  dgram->payload = (void*)malloc(SEGMENT_SIZE - sizeof(header));
  memset((void*)(dgram->payload), 0, SEGMENT_SIZE-sizeof(header));
  return (SEGMENT_SIZE-sizeof(header));
}

void bzero_dgram(datagram* dgram){
  
  memset((struct header*)(&(dgram->hdr)), 0, sizeof(header));
  memset((void*)(dgram->payload), 0, SEGMENT_SIZE-sizeof(header));
}

void print_packet_info(datagram dgram_out){
  printf("SeqNo=%u, AckNo=%u, Flag=", ntohs(dgram_out.hdr.seq_num), ntohs(dgram_out.hdr.ack_num));
  if(dgram_out.hdr.flag == (0)) printf("N/A ");
  if((dgram_out.hdr.flag & SYN) == (SYN)) printf("SYN-");
  if((dgram_out.hdr.flag & FIN) == (FIN)) printf("FIN-");
  if((dgram_out.hdr.flag & ACK) == (ACK)) printf("ACK-");
  if((dgram_out.hdr.flag & PROBE) == (PROBE)) printf("PROBE-");
  printf("\b, WindowSize=%d", htons(dgram_out.hdr.window_size));
  
}

void init_msgdr(struct msghdr* msg, struct iovec* iov, datagram* dgram, int payloadlen){
  
  memset(msg, 0, sizeof(struct msghdr));
  //memset(&msgrecv, 0, sizeof(struct msghdr));
  
  msg->msg_name = NULL;
  msg->msg_namelen = 0;
  msg->msg_iov = iov;
  msg->msg_iovlen = 2;
  iov[0].iov_base = (char*)(&(dgram->hdr));
  iov[0].iov_len = sizeof(header);
  iov[1].iov_base = dgram->payload;
  iov[1].iov_len = payloadlen;
}

typedef struct{
  datagram dgram;
  uint32_t ts_sent;
  int count_dup_acks;
  int retransmitted;
}slot;
slot* send_window=NULL;

int start=0, max_size, ssthreshold, cwnd, recv_wnd_size, slot_occupied;

void init_send_window(int size, int adv_wnd_size){
   max_size = size;
   send_window = (slot*)malloc(size*sizeof(slot));
   int i;
   for(i=0;i<size;i++){
     memset(&(send_window[i]), 0, sizeof(slot));
     init_dgram(&(send_window[i].dgram));
   }
   
   //ssthreshold = (int)ceil(0.75 * max_size);
   ssthreshold = adv_wnd_size;
   cwnd=1;
   recv_wnd_size = adv_wnd_size;
   slot_occupied = 0;
   printf("Max Sending Window size set to %d, cwnd=%d, ssthreshold set to client's advertised window size = %d\n", max_size, cwnd, ssthreshold);
}

int send_wnd_size(){
  return slot_occupied;
}

int max_send_wnd_size(){
  return min(max_size, min(cwnd, recv_wnd_size));
}

void dgram_cpy(datagram* dest, datagram* src, int payloadlen){
  
  memcpy(&(dest->hdr), &(src->hdr), sizeof(header));
  memcpy(dest->payload, src->payload, payloadlen);
}

int build_data_packet(datagram* dgram_out, FILE* fp){
  
    int n = fread(dgram_out->payload, 1, (SEGMENT_SIZE-sizeof(header)), fp);
    if (ferror(fp)){
      printf("Error reading from file!\n"); exit(-1);
    }else if (feof(fp)){
      
      dgram_out->hdr.flag = (FIN);
      dgram_out->hdr.window_size = htons(n);
      fclose(fp);
      return -1;
    }
    
    return 1;
}

void insert_send_window(datagram dgram_in, int payloadlen, struct rtt_info *rttinfo){
  
  if((max_send_wnd_size()-slot_occupied)<=0) return;
  int count, i=start;
  for(count=0;count<max_send_wnd_size();count++){ if(get_dgram_seqnum(send_window[(i+count)%max_size].dgram)==0) break; }
  
  int index = (i+count)%max_size;

  // i+count contains the first empty slot
  dgram_cpy(&(send_window[index].dgram), &dgram_in, payloadlen);
  send_window[index].ts_sent = rtt_ts(rttinfo);
  send_window[index].count_dup_acks = 0;
  send_window[index].retransmitted = 0;
  
  slot_occupied++;
  if(slot_occupied == max_size) printf("Sending window has reached its maximum capacity (locked), all %d slots are occupied\n", max_size);
}
// clears content into sending window up to packet ackno-1
void clear_send_window(int ackno){

  // if the window if already empty
  if(get_dgram_seqnum(send_window[start].dgram)==0) return;
  
  while(1){
    
    int done=0;
    
    while(get_dgram_seqnum(send_window[start].dgram) <= (ackno-1)){
      
      if(get_dgram_seqnum(send_window[start].dgram) == (ackno-1)) done=1;
      
      // erase slot and increase
      memset(send_window[start].dgram.payload, 0, (SEGMENT_SIZE-sizeof(header)));
      memset(&(send_window[start].dgram.hdr), 0, sizeof(header));
      send_window[start].count_dup_acks=0;
      send_window[start].retransmitted=0;
      send_window[start].ts_sent=0;
      
      start=((start+1)%max_size);
      slot_occupied--;

      if(done)break;
    }

    break;
  }
}

int is_cumulative_ack(int ackno){
  if(get_dgram_seqnum(send_window[start].dgram) < (ackno-1)) return 1;
  return 0;
}

void print_send_window(int max){
  int n, i=0;
  if(max<max_size) n=max;
  else n=max_size;
  printf("Send Window status (first %d elements): ",n);
  
  do{
    
    int seqno = get_dgram_seqnum(send_window[((i+start)%max_size)].dgram);
    
    if(seqno==0) printf("[ ]");
    else printf("[%d]", seqno);
    
    i++;
  }while( i<n );
  
  printf(" slot_occupied=%d, ssthreshold=%d, cwnd=%d, client_recv_wnd_size=%d\n", slot_occupied, ssthreshold, cwnd, recv_wnd_size);
}

int get_dgram_seqnum(datagram dgram){
  return ntohs(dgram.hdr.seq_num);
}

int get_dgram_acknum(datagram dgram){
  return ntohs(dgram.hdr.ack_num);
}

uint32_t get_ts(){
  return send_window[start].ts_sent;
}

int last_ack=0;
int get_last_ack(){
  // if the sending window is empty
  if(get_dgram_seqnum(send_window[start].dgram)==0) return last_ack;
  last_ack = get_dgram_seqnum(send_window[start].dgram);
  return last_ack;
}

void set_recv_wnd_size(int val){
  recv_wnd_size = val;
}

void get_oldest_pending_packet(datagram* dest){
  dgram_cpy(dest, &(send_window[start].dgram), SEGMENT_SIZE-sizeof(header));
}
// if count == 4, set retransmitted=1 and return 4;
int inc_count_dup_acks(){
  send_window[start].count_dup_acks++;
  return send_window[start].count_dup_acks;
}

void set_retransmitted(){
  // set the last pending packet as retransmitted
  send_window[start].retransmitted = 1;
}

int is_duplicate_ack(int ackno){
  // if we have received a previous ACK for this packet, that packet can only be at the starting position of the sending window
  if(get_dgram_seqnum(send_window[start].dgram) == ackno) return 1;
  return 0;
}

int is_retransmitted(int ackno){
  // if it's not a duplicate ACK (nor cumulative), and the packet ACKed has been retransmitted, return 1
  if( (get_dgram_seqnum(send_window[start].dgram)==(ackno-1)) && (send_window[start].retransmitted == 1) ) return 1;
  return 0;
}

void set_cwnd(int val){
  cwnd = val;
}
int get_cwnd(){
  return cwnd;
}

void set_ssthreshold(int val){
  ssthreshold = val;
}
int get_ssthreshold(){
  return ssthreshold;
}


void set_itimeval(struct itimerval* t, long sec, long usec){
  
  t->it_interval.tv_sec = 0;
  t->it_interval.tv_usec = 0;
  t->it_value.tv_sec = sec;
  t->it_value.tv_usec = usec;
}

// inserts a client_info structure into the list
client_list insert(client_list l, client_info cli_info){

	client_list t = (client_list)malloc(sizeof(client_item));
	memcpy(&(t->cli_info), &cli_info, sizeof(client_info));
	client_list temp=l;
	client_list p=NULL;

	while(temp!=NULL){
		p=temp;
		temp=(temp->next);
	}
	if(p==NULL) l=t;
	else (p->next)=t;
	t->next=NULL;
	return l;
}
// returns 1 if the list contains an entry with same client ip and port
int contains(client_list l, struct sockaddr_in client_ip, int client_port){

	client_list t = l;
	while(t!=NULL){
	
        if( (memcmp(&(t->cli_info.client_ip.sin_addr), &(client_ip.sin_addr), sizeof(struct in_addr))==0) && (t->cli_info.client_port == client_port) )
			return 1;
			t=t->next;
	}
	return 0;
}
// removes the client_info with that particular pid from the list and returns the list without that entry
client_list rmv(client_list l, pid_t pid){

	if(l==NULL) return l;
	client_list p,c,n; // previous, current, next
	p=NULL;
	c=l;
	n=c->next;

	while(c!=NULL){
        if(c->cli_info.pid == pid){
			
			free(c);
			if(p==NULL) l=n;
			else p->next=n;
			return l;
		}
		else{
			if(n==NULL) return l;
			n=n->next;
			p=c;
			c=c->next;
		}
	}
	// never executed
return l;
}
// for debug
void print_client_list(client_list l){

    char client_ip_str[16];
	client_info cli_info;
	int i=0;
	client_list temp = l;
	if(temp==NULL) printf("The client_info list is empty\n");
	while(temp!=NULL){
			cli_info = temp->cli_info;
			inet_ntop(AF_INET, &(cli_info.client_ip.sin_addr), client_ip_str, sizeof(client_ip_str));
			printf("> %d: ClientIP: %s, ClientPort: %d, ChildPID: %d\n", ++i, client_ip_str, ntohs(cli_info.client_port), cli_info.pid);
			temp=temp->next;
	}
}

// persistent timer functions (ms)
uint32_t timerval;
void init_persistent_timer(uint32_t* timeval){
  timerval = 1;
  *timeval = timerval;
  minmax_persistent_timer(timeval);
}

void minmax_persistent_timer(uint32_t* timerval){
  if(*timerval < MIN_PERSISTENT_TIMER) *timerval = MIN_PERSISTENT_TIMER;
  else if(*timerval > MAX_PERSISTENT_TIMER) *timerval = MAX_PERSISTENT_TIMER;
}

void double_persistent_timer(uint32_t* timeval){
  timerval << 1;
  *timeval = timerval;
  minmax_persistent_timer(timeval);
}

