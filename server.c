#include <stdio.h>
#include "unpifiplus.h"
#include "srv_utils.h"
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <math.h>
#include "unprtt_int.h"
#include <setjmp.h>
#include <sys/time.h>
#include "config.h"


client_list c_list=NULL;
datagram dgram_in;
struct msghdr msgrecv;
struct iovec iovrecv[2];
struct sockaddr_in cliaddr;
int listening_socket=0;
int count=0, i=0, j=0;
serv_strct* bind_info;
serv_input input;
char str[16];

void ftp_handler();
void sig_chld(int signo);
void sig_alrm(int signo);
sigjmp_buf jmpbuf;


int main(int argc, char** argv){
 
  struct ifi_info *ifi, *temp;
  struct sockaddr_in* server_ip = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
  struct sockaddr_in* server_nm = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
  struct sockaddr_in serv_addr;
  
  printf("\nReading inputs from 'server.in':\n");
  serv_read_input("server.in", &input);
  ifi= get_ifi_info_plus(AF_INET, 1);
  // obtain number of interfaces
  temp=ifi;
  for(; temp!=NULL; temp=temp->ifi_next){count++;}
  bind_info = (serv_strct*)malloc(count*sizeof(serv_strct));
  
  temp=ifi;
  while(temp!=NULL){
    
       if ( ((server_ip = (struct sockaddr_in*)temp->ifi_addr) == NULL) || ((server_nm = (struct sockaddr_in*)temp->ifi_ntmaddr) == NULL)){
	 printf("Error reading ifi_info\n");
	 temp=temp->ifi_next;
	 continue;
       }
       
      // UDP socket
      if((bind_info[i].skfd = socket(AF_INET, SOCK_DGRAM, 0))<0){
	printf("Termination due to socket() error\n");
	exit(-1);
      }
  
      // init data
      memset((char *)&bind_info[i].server_ip, 0, sizeof(struct sockaddr_in));
      memset((char *)&bind_info[i].netmsk, 0, sizeof(struct sockaddr_in));
      memset((char *)&bind_info[i].subntadd, 0, sizeof(struct sockaddr_in));
      bind_info[i].server_ip.sin_family = AF_INET;
      bind_info[i].server_ip.sin_addr.s_addr = server_ip->sin_addr.s_addr;
      bind_info[i].server_ip.sin_port = htons(input.port); 
      bind_info[i].netmsk.sin_addr.s_addr = server_nm->sin_addr.s_addr;
      bind_info[i].subntadd.sin_addr.s_addr = bind_info[i].server_ip.sin_addr.s_addr & bind_info[i].netmsk.sin_addr.s_addr;

      // for debug
      const int on=1;
      if(setsockopt(bind_info[i].skfd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on))<0){
	printf("Termination due to setsockopt() error\n");
	exit(-1);
      }
      
      // bind
      if(bind(bind_info[i].skfd,(struct sockaddr*) &(bind_info[i].server_ip), sizeof(struct sockaddr_in))<0){
	printf("Termination due to bind() error\n");
	exit(-1);
      }
      
      temp=temp->ifi_next;
      i++;
  }
  free_ifi_info_plus(ifi);
  
  for(i=0;i<count;i++){
    if(inet_ntop(AF_INET, &bind_info[i].server_ip.sin_addr, str, sizeof(str))<=0){printf("Cannot convert Server IP address to readable form\n");exit(-1);}
    printf("UDP Socket bound to IP address=%s and port=%d. ", str, ntohs(bind_info[i].server_ip.sin_port));
    if(inet_ntop(AF_INET, &bind_info[i].netmsk.sin_addr, str, sizeof(str))<=0){printf("Cannot convert Netmask address to readable form\n");exit(-1);}
    printf("Netmask address=%s and ", str);
    if(inet_ntop(AF_INET, &bind_info[i].subntadd.sin_addr, str, sizeof(str))<=0){printf("Cannot convert Subnet Address to readable form\n");exit(-1);}
    printf("Subnet address=%s\n", str);
  }
  
 
  int payloadlen = init_dgram(&dgram_in);
  pid_t pid;
  init_msgdr(&msgrecv, iovrecv, &dgram_in, payloadlen);
  msgrecv.msg_name = &cliaddr;
  msgrecv.msg_namelen = sizeof(cliaddr);
  client_info entry;
 
  // select
  int maxsd=bind_info[0].skfd;
  for(i=1;i<count;i++){  
    if(bind_info[i].skfd > maxsd)
    maxsd = bind_info[i].skfd;
  }
  maxsd++;
  fd_set myset;
  FD_ZERO(&myset);
  
  signal(SIGCHLD, sig_chld); 
  printf("\nWaiting for incoming packets...\n");
  
  for(;;){
    
    for(i=0;i<count;i++){
      FD_SET(bind_info[i].skfd, &myset);
    }

    if(select(maxsd, &myset, NULL, NULL, NULL)<0){
      if(errno==EINTR)
	continue;
      else{
	perror("select failed");
	exit(-1);
      } 
    }

    // Check which of the sockets has triggered select
    for(i=0;i<count;i++){
      
      if (FD_ISSET(bind_info[i].skfd, &myset)){
	
	if(recvmsg(bind_info[i].skfd,&msgrecv,0)<0){
	  printf("recvmsg() error on socket %d\n", bind_info[i].skfd);
	  continue;
	}
	
	printf("Packet: "); print_packet_info(dgram_in); printf(" RECEIVED\n");
	
	// Obtain client ip address and port number
	if(inet_ntop(AF_INET, &(cliaddr.sin_addr), str, sizeof(str))<=0){printf("Cannot convert Client IP address to readable form\n");continue;}
	printf("Incoming packet from client %s (port %d)\n", str, ntohs(cliaddr.sin_port));
	
	// check if client is being already served -> continue
	if (contains(c_list, cliaddr, cliaddr.sin_port)>0){
	  client_list temp = c_list;
	  while(temp!=NULL){
	    
	    if( (memcmp(&(temp->cli_info.client_ip.sin_addr), &(cliaddr.sin_addr), sizeof(struct in_addr))==0) && (temp->cli_info.client_port == cliaddr.sin_port) ){
	      pid = temp->cli_info.pid;
	      break;
	    }
	    temp=temp->next;
	  }
	  printf("This client is already being served by process with PID=%d\n", pid);
	  continue;
	}
	
	// New client	

	if ( (pid=fork()) == 0){	
	  /* close other sockets in the forked child and start reading data */
	  for(j=0;j<count;j++){
	    if(bind_info[i].skfd != bind_info[j].skfd) close(bind_info[j].skfd);
	  }

	  listening_socket = bind_info[i].skfd;
	  
	  // file transfer code
	  ftp_handler();
	  
	  exit(0);		
	}
	
	// insert new client into the list	
	entry.client_ip = cliaddr;
	entry.client_port = cliaddr.sin_port;   // initialised to -1. clients connect datagram should contain this 									//info
	entry.pid = pid;
	c_list = insert(c_list, entry); //add client to the structure
	printf("New client inserted into the active clients' list\n");

	print_client_list(c_list);
	
      } // FD_ISSET
    } // for each interface 
  } // select() infinite loop 
  
  printf("Unreachable code executed! (post infinite select() loop)\n");
  exit(-1);
}


void sig_chld(int signo){

  int  stat;
  pid_t zombie_chld;
  while( (zombie_chld = waitpid(-1, NULL, WNOHANG))>0 ){  
  c_list = rmv(c_list, zombie_chld);
  printf("Child %d has terminated, its PID and client information has been removed from the \"active clients\" list\n", zombie_chld);
  }
  return;
}


void sig_alrm(int signo){
  siglongjmp(jmpbuf,1);
}


void ftp_handler(){
  
  // check if the packet is correct
  if(dgram_in.hdr.flag != (SYN)){
    printf("Invalid packet received from the client (not a SYN), Child process terminating...\n");
    return;
  }
  
  // packet is correct
  printf("Valid SYN packet received (");
 
  struct sockaddr_in servaddr;
  struct in_addr subnet_cli, subnet_serv;
  serv_strct local_info;
  int is_local=0, conn_sock, seq_num=0;
  
  // Get filename
  char filename[MAX_LENGTH]; memset(filename,0,sizeof(filename));
  strcpy(filename, dgram_in.payload);
  printf("file requested: %s)\n", filename);
  
  FILE* f;
  if(NULL == (f = fopen(filename, "r"))) {
    printf("Cannot open file %s!\nChild process terminating..\n", filename); return;
  }
  fclose(f);
  
  // Get Receiving window size
  int recv_wndw_size = ntohs(dgram_in.hdr.window_size);
  
  // Check if client is local
  
  for(i=0;i<count;i++){
     if(bind_info[i].skfd == listening_socket) local_info=bind_info[i];
  }
  
  if(inet_pton(AF_INET, "127.0.0.1", &(servaddr.sin_addr))<=0){printf("inet_pton() error\n");exit(-1);}
  // if the client address is 127.0.0.1
  if(memcmp(&(cliaddr.sin_addr), &(servaddr.sin_addr), sizeof(struct in_addr))==0){
    is_local=1;
    printf("The client is located on the same host (accessed through loopback address)");
  }
  else{
    
    for(i=0;i<count;i++){
      
      if(memcmp(&(cliaddr.sin_addr), &(bind_info[i].server_ip.sin_addr), sizeof(struct in_addr))==0){
	is_local=1;
	printf("The client is located on the same host (accessed through same IP)");
      }
      
      subnet_serv = bind_info[i].subntadd.sin_addr;
      subnet_cli.s_addr = cliaddr.sin_addr.s_addr & bind_info[i].netmsk.sin_addr.s_addr;
      
      if( memcmp(&subnet_cli, &subnet_serv, sizeof(struct in_addr))==0 ){
	is_local=1;
	printf("The client is local to my (extended) Ethernet");
	break;
      }
    }
  }
  
  const int on=1;
  if(is_local){  
    if(setsockopt(listening_socket, SOL_SOCKET, SO_DONTROUTE, &on, sizeof(on))<0){
      printf("Termination due to setsockopt() error\n");
      exit(-1);
    }
    printf(". SO_DONTROUTE flag set on listening socket.\n");
  }
  else{
    printf("The client is not local to my (extended) Ethernet nor located on the same host\n");
  }

  // Get UDP socket for file transfer
  
  memset((char *)&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr = local_info.server_ip.sin_addr;
  servaddr.sin_port = 0; 
  
  if((conn_sock=socket(AF_INET, SOCK_DGRAM, 0))<0){
    printf("Termination due to socket() error\n");
    exit(-1);
  }
  
  if(is_local){  
  if(setsockopt(conn_sock, SOL_SOCKET, SO_DONTROUTE, &on, sizeof(on))<0){
      printf("Termination due to setsockopt() error\n");
      exit(-1);
    }
    printf("\"Connection\" socket created (SO_DONTROUTE flag set) ");
  }
  else
    printf("\"Connection\" socket created ");
  
  // bind to new ephemeral port
  if(bind(conn_sock,(struct sockaddr*) &servaddr, sizeof(servaddr))<0){printf("Termination due to bind() error\n");exit(-1);}
  
  // Obtain local port and ip address bound
  unsigned int len = sizeof(servaddr);
  if(getsockname(conn_sock, (struct sockaddr*)&servaddr, &len)<0){printf("Termination due to getsockname() error\n");exit(-1);}
  if(inet_ntop(AF_INET, &(servaddr.sin_addr), str, sizeof(str))<=0){printf("Cannot convert local IP address to readable form\n");exit(-1);}
  printf("and bound to to local IP address %s and port %d\n\n", str, ntohs(servaddr.sin_port));
  
  // Start handshaking and transmission
  datagram dgram_out;
  int payloadlen = init_dgram(&dgram_out);
  struct msghdr msgsend;
  struct iovec iovsend[2];
  memset(&msgsend, 0, sizeof(struct msghdr));
  init_msgdr(&msgsend, iovsend, &dgram_out, payloadlen);
  msgsend.msg_name = &cliaddr;
  msgsend.msg_namelen = sizeof(cliaddr);
  struct timeval timeout;
  // init timeout
  static struct rtt_info rttinfo;
  static int rttinit = 0;
  // init estimators and initial timestamp
  rtt_init(&rttinfo);
  rtt_d_flag = 1;
  timeout.tv_sec = rtt_start(&rttinfo)/1000;
  timeout.tv_usec =  1000 * (rtt_start(&rttinfo) % 1000);
  
  // Send SYN-ACK with port number to listening socket (retransmission occur to both sockets)
  
  // select init
  fd_set rset;
  int res=-1;
  int maxfd=0;
  
  rtt_newpack(&rttinfo); // reset every time we receive a valid ACK (in fact we only retransmit with timeout the oldest pending datagram) (when we get 3 dup acks, we retransmit that particular datagram)
  bzero_dgram(&dgram_out);
  dgram_out.hdr.seq_num = htons(seq_num);
  dgram_out.hdr.flag = (SYN | ACK);
  int new_port = servaddr.sin_port;    
  memcpy((int*)(dgram_out.payload), &new_port, sizeof(new_port));

sendSYNACK:
  if(sendmsg(listening_socket, &msgsend, 0)<0){
    printf("sendmsg() error for packet %d\n", get_dgram_seqnum(dgram_out)); goto sendSYNACK;
  }
  else{
    printf("Packet: "); print_packet_info(dgram_out); printf(" (with new port number %d) SENT\n", ntohs(*((int*)dgram_out.payload)));
  }
    
  // Ensure retransmission with timeout for the first packet sent by the server: SYN-ACK
  
  // Waiting for first ACK to start real file transfer
  FD_ZERO(&rset);
  for(;;){

    FD_SET(conn_sock, &rset);
    maxfd=conn_sock+1;
    
    res = select(maxfd, &rset, NULL, NULL, &timeout);
    if(res<0){
      if (errno==EINTR) continue;
      else {
      printf("select() error errno=%d\n", errno);exit(-1);}
    }

    // SYN-ACK timed out
    if(res==0){
      if(rtt_timeout(&rttinfo)<0){
	printf("No response from client on new connected socket, giving up...\n");
	rttinit = 0;
	return;
      }
      printf("SYN-ACK packet (with port number) timed out, retransmitting on both listening and new connection socket...\n");
      
      if(sendmsg(listening_socket, &msgsend, 0)<0) printf("sendmsg() error while retransmitting packet %d (SYN-ACK) on parent's listening socket\n", get_dgram_seqnum(dgram_out));
      if(sendmsg(conn_sock, &msgsend, 0)<0) printf("sendmsg() error while retransmitting packet %d (SYN-ACK) on connected socket\n", get_dgram_seqnum(dgram_out));
      printf("Packet: "); print_packet_info(dgram_out); printf(" (with new port number %d) SENT on both sockets\n", ntohs(*((int*)dgram_out.payload)));
      timeout.tv_sec = rtt_start(&rttinfo)/1000;
      timeout.tv_usec =  1000 * (rtt_start(&rttinfo) % 1000);
      continue;
    }
    
    if(FD_ISSET(conn_sock, &rset)){	
      
      if(recvmsg(conn_sock, &msgrecv, 0)<0){printf("recvmsg_sim() error, Child process terminating..\n"); exit(-1);}
      
      // check if packet is correct
      if( (dgram_in.hdr.flag != (ACK)) && (ntohs(dgram_in.hdr.ack_num) != 1)){
	printf("Invalid Packet received (ACK1 expected): "); print_packet_info(dgram_in); printf("\n"); continue;
      }
      
      printf("Packet: "); print_packet_info(dgram_in); printf(" RECEIVED\n");

      // Valid ACK1 packet received
      rtt_newpack(&rttinfo);
      
      // Now the client has disconnected his socket from the old listening socket, I can issue a connect without incurring in ECONNREFUSED errors
      // connect new socket to peer
      if(connect(conn_sock, (struct sockaddr*)&cliaddr, sizeof(cliaddr))<0){
	printf("Termination due to connect() error\n");
	exit(-1);
      }
      
      if(inet_ntop(AF_INET, &cliaddr.sin_addr, str, sizeof(str))<=0){printf("Cannot convert Client IP address to readable form\n");exit(-1);}
      printf("Socket connected to client IP address %s and port %d\n", str, ntohs(cliaddr.sin_port));
      

      // socket is now connected
      msgsend.msg_name = NULL;
      msgsend.msg_namelen = 0;
    
      // close listening_socket inherited from the parent
      close(listening_socket);

      break;
    }
  }
  
  printf("3-way Handshaking completed, file transfer started\n");

  signal(SIGALRM, sig_alrm);
  struct itimerval to;
  uint32_t pers_timer_ms;
  set_itimeval(&to, rtt_start(&rttinfo)/1000, 1000 * (rtt_start(&rttinfo) % 1000));
  int flag_PROBE=0, flag_EOF=0;
  init_send_window(input.send_window_size, recv_wndw_size);
  FILE* fp;
  if(NULL == (fp = fopen(filename, "r"))) {
    printf("Cannot open file %s!\nChild process terminating..\n", filename); return;
  }
  int n=0, n_limit=0; // n and n_limit are used for congestion control. They determine when to increase cwnd

  
  while(1){
    
    // build segments, send and insert into sending window
    if( (send_wnd_size() < max_send_wnd_size()) && (!flag_EOF) ){
      int i, c = max_send_wnd_size()-send_wnd_size();
      for(i=0;(i<c) && (!flag_EOF);i++){
	
	bzero_dgram(&dgram_out);
	if(build_data_packet(&dgram_out, fp)<0) flag_EOF=1;
	dgram_out.hdr.seq_num = htons(++seq_num);
	
	if(sendmsg(conn_sock, &msgsend, 0)<0) printf("sendmsg() error for packet %d - Is the peer still connected?\n", get_dgram_seqnum(dgram_out));
	printf("Packet: "); print_packet_info(dgram_out); printf(" SENT\n");
	
	insert_send_window(dgram_out, SEGMENT_SIZE-sizeof(header), &rttinfo);
      }
      printf("%d packets sent to client -> ", c);
      print_send_window(DBG_SEND_WNDW);
    }
    
    // set timer for oldest in-flight packet
    setitimer(ITIMER_REAL, (&to),0);
    
    if(sigsetjmp(jmpbuf,1)!=0){
     
      // if we timed out because of a locked recv window
      if(flag_PROBE){
	
	printf("Persistent Timer expired: no window updates received from client after detecting a locked receiving window (size=0), sending PROBE packet\n");
	dgram_out.hdr.flag = (PROBE);
	
	if(sendmsg(conn_sock, &msgsend, 0)<0) printf("sendmsg() error for packet %d - Is the peer still connected?\n", get_dgram_seqnum(dgram_out));
	printf("Packet: "); print_packet_info(dgram_out); printf(" SENT\n");
	double_persistent_timer(&pers_timer_ms);
	set_itimeval(&to, pers_timer_ms/1000, 1000*(pers_timer_ms%1000));
      }
      else{
	
	if(rtt_timeout(&rttinfo)<0){
	  printf("Max number of retransmissions reached, Child process terminating..\n"); exit(0);
	}
	// not time to give up yet
	printf("Timeout triggered for packet %d (oldest pending message), retransmitting...\n", get_last_ack());
	
	bzero_dgram(&dgram_out);
	get_oldest_pending_packet(&dgram_out);
	
	if(sendmsg(conn_sock, &msgsend,0)<0) printf("sendmsg() error for packet %d - Is the peer still connected?\n", get_dgram_seqnum(dgram_out));
	printf("Packet: "); print_packet_info(dgram_out); printf(" SENT\n");
	set_retransmitted();
	
	set_itimeval(&to, rtt_start(&rttinfo)/1000, 1000 * (rtt_start(&rttinfo) % 1000));
	
	// update congestion control variables
	set_ssthreshold( (int)ceil( ((double)get_cwnd())/2 ) ); // ssthreshold = cwnd/2
	set_cwnd(1); // cwnd = 1
	n=get_cwnd(); n_limit=(2*n);
	printf("Restarting Slow Start mechanism, cwnd=%d, ssthreshold=%d\n", get_cwnd(), get_ssthreshold());
      }
      
      setitimer(ITIMER_REAL, (&to),0);
    }
    
    do{
      if(recvmsg(conn_sock, &msgrecv, 0)<0){printf("recvmsg_sim() error, Child process terminating..\n"); exit(-1);}
    }while(get_dgram_acknum(dgram_in) < get_last_ack()); // we return only if a valid packet has been received
    
    printf("Packet: "); print_packet_info(dgram_in); printf(" RECEIVED\n");
    
    // valid packet received
    
    setitimer(ITIMER_REAL, 0, 0);
    rtt_newpack(&rttinfo);
    
    // Congestion control variables
    //n++;
    n += get_dgram_acknum(dgram_in) - get_last_ack();
    
    
    if( (!flag_PROBE) && (!is_retransmitted(get_dgram_acknum(dgram_in))) && (!is_cumulative_ack(get_dgram_acknum(dgram_in))) && (!is_duplicate_ack(get_dgram_acknum(dgram_in))) ){
      
      uint32_t current_ts = rtt_ts(&rttinfo);
      uint32_t sent_ts = get_ts();
      rtt_stop(&rttinfo, current_ts - sent_ts);
      
      printf("ACK%d - not ambiguous - used to update estimators and evaluate RTO (%lu ms)\n", get_dgram_acknum(dgram_in), (unsigned long)rtt_start(&rttinfo));
    }
    else{
      if(flag_PROBE) printf("Duplicate ACKs received due to PROBE packets cannot be used to update estimators and estimated RTO\n");
      else if(is_cumulative_ack(get_dgram_acknum(dgram_in))) printf("Cumulative ACKs cannot be uniquely associated to any packet in the sending window, hence is not possible to update estimators\n");
      else if(is_retransmitted(get_dgram_acknum(dgram_in))) printf("Packet %d has been retransmitted, hence we cannot use it to update RTO and estimators (Karn's modification)\n", get_dgram_acknum(dgram_in)-1);
      else if(is_duplicate_ack(get_dgram_acknum(dgram_in))) printf("Duplicate ACKs cannot be uniquely associated to any packet in the sending window (which packet triggered the ACK), hence is not possible to update estimators\n");
    }
    
    // Congestion control
    if(get_cwnd() < get_ssthreshold()){
      set_cwnd(get_cwnd()+1); n=get_cwnd(); n_limit=(2*n);
      
      if(get_cwnd() == get_ssthreshold()){
	printf("cwnd incremented (it reached ssthreshold=%d): entering Congestion Avoidance; packet %d needs to be ACKed before incrementing cwnd again\n", get_ssthreshold(), get_dgram_acknum(dgram_in)+get_cwnd()-1);
      }
      else{
	printf("Slow Start: cwnd below ssthreshold; cwnd incremented (now %d)\n", get_cwnd());
      }
    }
    else{
      if(n>=n_limit){
	// cwnd cannot grow esponentially! we need to keep it at a maximum value so that when a problem occurs (timeout or dup acks), it shrinks appropriately (doesn't get half a huge size, because the sender is always limited by the window size specified in server.in)
	if(get_cwnd() <= max_send_wnd_size()){
	  set_cwnd(get_cwnd()+1);
	  set_ssthreshold(get_cwnd());
	  n=get_cwnd();
	  n_limit=(2*n);
	  printf("Congestion Avoidance: cwnd over ssthreshold; cwnd incremented (now %d)", get_cwnd());
	  
	  printf(" (packet %d needs to be ACKed before incrementing cwnd again)\n", get_dgram_acknum(dgram_in)+get_cwnd()-1);
	}
	else{
	  printf("Cannot increase sending window size: cwnd reached the maximum size allowed defined by 'server.in' (%d slots)\n", input.send_window_size);
	}
      }
      else{
	printf("Congestion Avoidance: cwnd over ssthreshold; still %d packets to be ACKed before incrementing the congestion window\n", n_limit-n);
      }
    }
    // end congestion control
    
    // if FIN-ACK has been received, break
    if(dgram_in.hdr.flag == (FIN|ACK)){
      printf("FIN-ACK received from the client, file transfer completed, sending last ACK to complete the closing procedure..\n");
      break;
    }
    
    clear_send_window(get_dgram_acknum(dgram_in));
    set_recv_wnd_size(ntohs(dgram_in.hdr.window_size));
    print_send_window(DBG_SEND_WNDW);
    
    // if this ACK is a duplicate ACK caused by a PROBE packet, we do not increment the ACK counter
    if( (!flag_PROBE) && (inc_count_dup_acks() == 4)){
      printf("Received 3 duplicate ACKs for packet %d, retransmitting...\n", get_dgram_acknum(dgram_in));
      bzero_dgram(&dgram_out);
      get_oldest_pending_packet(&dgram_out);
      
      if(sendmsg(conn_sock, &msgsend, 0)<0) printf("sendmsg() error for packet %d - Is the peer still connected?\n", get_dgram_seqnum(dgram_out));
      printf("Packet: "); print_packet_info(dgram_out); printf(" SENT\n");
      set_retransmitted();
      
      set_cwnd( (int)ceil( ((double)get_cwnd())/2) );
      set_ssthreshold(get_cwnd());
      n=get_cwnd();
      n_limit=(2*n);
      printf("Activating Fast Recovery mechanism, cwnd=%d, ssthreshold=%d\n", get_cwnd(), get_ssthreshold());
    }
    
    flag_PROBE = 0;
    
    //
    
    if(max_send_wnd_size() == 0){
      printf("Client's receiving window is full (locked); activating timer for PROBE packet\n");
      flag_PROBE=1;
      init_persistent_timer(&pers_timer_ms);
      set_itimeval(&to, pers_timer_ms/1000, 1000*(pers_timer_ms%1000));
    }
    else{
      set_itimeval(&to, rtt_start(&rttinfo)/1000, 1000 * (rtt_start(&rttinfo) % 1000));
    }
  } // infinite loop
  
  bzero_dgram(&dgram_out);
  dgram_out.hdr.seq_num = htons(++seq_num);
  dgram_out.hdr.flag = (ACK);
  if(sendmsg(conn_sock, &msgsend, 0)<0){
    printf("sendmsg() error for packet %d\n", get_dgram_seqnum(dgram_out));
  }
  else{
    printf("(Last) Packet: "); print_packet_info(dgram_out); printf(" SENT\n\nFTP Service Completed, Child process terminating..\n");
  }

}
