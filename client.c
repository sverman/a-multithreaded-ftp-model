#include <stdio.h>
#include "unpifiplus.h"
#include "cli_utils.h"
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <errno.h>
#include <pthread.h>
#include <math.h>
#include "config.h"

#define CLIENT_TIMEOUT 3


void closing(int fd){
  
  void destroy_recv_window();
  close(fd);
}


int sock_fd;
struct sockaddr_in cliaddr, cliaddr2, servaddr;
int seq_num=0;
int expected_dgram=0;
unsigned int len;
datagram dgram_out, dgram_in;
int payloadlen;
struct iovec iovsend[2], iovrecv[2];
struct msghdr msgsend, msgrecv;
char client_ip_str[16];
char 	server_ip_str[16];
cli_input input;
datagram* recv_wndw = NULL;



int main(int argc, char** argv){
  
  struct ifi_info *ifi;
  struct sockaddr_in client_ip;
  struct sockaddr_in server_ip;
  int flag = 0;
  
  printf("\nReading inputs from 'client.in':\n");
  cli_read_input("client.in", &input);
  
  recv_wndw = (datagram*)malloc(input.recv_window_size*sizeof(datagram));
  int j; for(j=0;j<input.recv_window_size;j++){
    init_dgram(&(recv_wndw[j]));
  }
  
  ifi= get_ifi_info_plus(AF_INET, 1);
  printf("\nList of all IP addresses and associated network masks:\n");
  print_ifi_info_plus(ifi); printf("\n");
  select_appropriate_ip_addresses(ifi, &client_ip, &server_ip, input.server_ip_str, &flag);
  free_ifi_info_plus(ifi);
    // I print the IPclient and IPserver addresses selected
  if(inet_ntop(AF_INET, &client_ip.sin_addr, client_ip_str, sizeof(client_ip_str))<=0){printf("Cannot convert Server IP address to readable form\n");exit(-1);}
  if(inet_ntop(AF_INET, &server_ip.sin_addr, server_ip_str, sizeof(server_ip_str))<=0){printf("Cannot convert Client IP address to readable form\n");exit(-1);}
  printf("  Client IP address selected: %s\n  Server IP address selected: %s\n\n", client_ip_str, server_ip_str);
  
  // UDP socket
  if((sock_fd=socket(AF_INET, SOCK_DGRAM, 0))<0){
    printf("Termination due to socket() error\n");
    exit(-1);
  }
  
  // init data
  memset((char *)&cliaddr, 0, sizeof(cliaddr));
  cliaddr.sin_family = AF_INET;
  cliaddr.sin_addr.s_addr = client_ip.sin_addr.s_addr;
  cliaddr.sin_port = 0;
   
  // For Debug
  const int on=1;
  if(setsockopt(sock_fd, SOL_SOCKET, SO_REUSEADDR, &on, sizeof(on))<0){
    printf("Termination due to setsockopt() error\n");
    exit(-1);
  }
  
  // bind
  if(bind(sock_fd,(struct sockaddr*) &cliaddr, sizeof(cliaddr))<0){
    printf("Termination due to bind() error\n");
    exit(-1);
  }
  
  // Obtain local port and ip address bound
  len = sizeof(cliaddr2);
  if(getsockname(sock_fd, (struct sockaddr*)&cliaddr2, &len)<0){
    printf("Termination due to getsockname() error\n");
    exit(-1);
  }
  if(inet_ntop(AF_INET, &(cliaddr2.sin_addr), client_ip_str, sizeof(client_ip_str))<=0){printf("Cannot convert Client IP address to readable form\n");exit(-1);}
  printf("UDP socket bound to local IP address %s and ephemeral port %d", client_ip_str, ntohs(cliaddr2.sin_port));
  
  // set option
  if(flag!=0){  
    if(setsockopt(sock_fd, SOL_SOCKET, flag, &on, sizeof(on))<0){
      printf("Termination due to setsockopt() error\n");
      exit(-1);
    }
    printf(" (SO_DONTROUTE flag set)");
  }
  printf("\n");
  
  // init
  memset((char *)&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_addr.s_addr = server_ip.sin_addr.s_addr;
  servaddr.sin_port = htons(input.serv_port); 
  
  // connect to server
  if(connect(sock_fd, (struct sockaddr*)&servaddr, sizeof(servaddr))<0){
    printf("Termination due to connect() error errno=%d\n", errno);
    exit(-1);
  }
  
  // Obtain server ip address and well-known port number
  len = sizeof(servaddr);
  if(getpeername(sock_fd, (struct sockaddr*)&servaddr, &len)<0){
    printf("Termination due to getpeername() error\n");
    exit(-1);
  }

  if(inet_ntop(AF_INET, &(servaddr.sin_addr), server_ip_str, sizeof(server_ip_str))<=0){printf("Cannot convert Server IP address to readable form\n");exit(-1);}
  printf("Socket connected to server IP address %s and well-known port %d\n\n", server_ip_str, ntohs(servaddr.sin_port));

  
  // Init
  init_dropper_sim(input.seed, input.p);
  init_recv_window(input.recv_window_size, recv_wndw);
  
  payloadlen = init_dgram(&dgram_out);
  init_dgram(&dgram_in);
  init_msgdr(&msgsend, iovsend, &dgram_out, payloadlen);
  init_msgdr(&msgrecv, iovrecv, &dgram_in, payloadlen);
  
  // CREATE PRODUCER AND CONSUMER THREADS
  pthread_t prod_t; 
  pthread_t cons_t; 
  void *protocol_layer();
  void *app_layer();
  
  pthread_create(&prod_t,NULL,protocol_layer,NULL);
  pthread_create(&cons_t,NULL,app_layer,NULL);
  pthread_join(prod_t,NULL);
  pthread_join(cons_t,NULL);
  
  printf("MAIN: Both Consumer and Producer threads have terminated\n\tClosing...\n\n");
  
  closing(sock_fd);
  
  return 1;
}







// Thread variables
  pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_t space_available = PTHREAD_COND_INITIALIZER;
  pthread_cond_t data_available = PTHREAD_COND_INITIALIZER;


void *protocol_layer(){
  
  printf("PRODUCER thread started\n");
  
  // select init
  struct timeval timeout; timeout.tv_sec = CLIENT_TIMEOUT; timeout.tv_usec=0;
  fd_set rset;
  int res=-1;
  int maxfd=0;  
  
  // Send first datagram, SYN with filename and receiving window size
  bzero_dgram(&dgram_out);
  dgram_out.hdr.seq_num = htons(seq_num); seq_num++;
  pthread_mutex_lock(&lock);
  dgram_out.hdr.window_size = htons(calculate_advertised_wnd_size());
  pthread_mutex_unlock(&lock);
  dgram_out.hdr.flag = (SYN);
  strcpy((char*)(dgram_out.payload), input.file_name);
  
  sendmsg_sim(sock_fd, dgram_out, &msgsend);
  
  // Waiting for SYN-ACK with new port number
  FD_ZERO(&rset);
  for(;;){

    FD_SET(sock_fd, &rset);
    maxfd=sock_fd+1;
    
    res = select(maxfd, &rset, NULL, NULL, &timeout);
    if(res<0){
      if (errno==EINTR) continue;
      else {
      printf("select() error errno=%d\n", errno);exit(-1);}
    }

    // SYN timed out
    if(res==0){
      printf("SYN packet timed out, retransmitting..\n");
      dgram_out.hdr.seq_num = htons(seq_num); seq_num++;
      sendmsg_sim(sock_fd, dgram_out, &msgsend);
      timeout.tv_sec = CLIENT_TIMEOUT; timeout.tv_usec=0;
      continue;
    }
    
    if(FD_ISSET(sock_fd, &rset)){
      
      if(recvmsg_sim(sock_fd, &dgram_in, &msgrecv)<0) continue;
      
      // check if packet is correct
      if( (dgram_in.hdr.flag != (SYN | ACK)) && (ntohs(dgram_in.hdr.seq_num) < expected_dgram)){
	printf("Invalid Packet received (SYN-ACK expected): "); print_packet_info(dgram_in); printf("\n"); continue;
      }

      // Packet contains SYN-ACK flag and port number
      expected_dgram++;

      break;
    }
  }
  
  // Read packet
  int new_port = ntohs(*((int*)dgram_in.payload));
  
  // get client's address information but change port to new one
  len = sizeof(servaddr);
  if(getpeername(sock_fd, (struct sockaddr*)&servaddr, &len)<0){
    printf("Termination due to getpeername() error\n");
    exit(-1);
  }
  servaddr.sin_port = htons(new_port); 
  
  // connect to server
  if(connect(sock_fd, (struct sockaddr*)&servaddr, sizeof(servaddr))<0){
    printf("Termination due to connect() error\n");
    exit(-1);
  }
  if(inet_ntop(AF_INET, &(servaddr.sin_addr), server_ip_str, sizeof(server_ip_str))<=0){printf("Cannot convert Server IP address to readable form\n");exit(-1);}
  printf("UDP socket reconnected to server IP address %s and port %d\n", server_ip_str, ntohs(servaddr.sin_port));
  
  msgsend.msg_name = NULL; // connected
  msgsend.msg_namelen = 0;

  // Send ACK1 to let the server know we're ready to receive the first data packet. Note that this can get lost: it's the server's responsability to provide retransmissions
  bzero_dgram(&dgram_out);
  dgram_out.hdr.seq_num = htons(seq_num); seq_num++;
  pthread_mutex_lock(&lock);
  dgram_out.hdr.window_size = htons(calculate_advertised_wnd_size());
  pthread_mutex_unlock(&lock);
  dgram_out.hdr.flag = (ACK);
  dgram_out.hdr.ack_num = htons(expected_dgram);
  
  sendmsg_sim(sock_fd, dgram_out, &msgsend);
  
  // Cycle [receive-sendACK-process] (sliding window)
  FD_ZERO(&rset);
  for(;;){
    
    FD_SET(sock_fd, &rset);
    maxfd=sock_fd+1;    
    
    // SELECT
    res = select(maxfd, &rset, NULL, NULL, NULL); // no timeouts during file transfer on the client side: the client waits untill all the packets have been received    
    
    
    if(res<0){
      if (errno==EINTR) continue;
      else {
      printf("select() error errno=%d\n", errno);
      exit(-1);
      }
    }
    
    // socket is readable
    if(FD_ISSET(sock_fd, &rset)){	

      if(recvmsg_sim(sock_fd, &dgram_in, &msgrecv)<0)continue;
      
      // check if packet is correct
      if(dgram_in.hdr.flag == (PROBE)){
	
	printf("PROBE packet received. Sending duplicate ACK..\n");
      }
      else if(get_dgram_seqnum(dgram_in) < expected_dgram){
	
	printf("Invalid (out of valid range) Packet received: SeqNo=%d\n",get_dgram_seqnum(dgram_in));
      }
      else{
	
	pthread_mutex_lock(&lock);
	if(calculate_advertised_wnd_size()==0)	// if recv window is full, we suspend
	  pthread_cond_wait(&space_available,&lock);
	
	int res;
	if( (res = process_packet(dgram_in))<0){
	  
	  printf("All packets up to FIN have been received in order. Sending FIN-ACK and waiting for last ACK\n");
	  
	  bzero_dgram(&dgram_out);
	  dgram_out.hdr.seq_num = htons(seq_num); seq_num++;
	  dgram_out.hdr.window_size = htons(calculate_advertised_wnd_size());
	  dgram_out.hdr.flag = ((FIN | ACK));
	  expected_dgram = calculate_expected_dgram();
	  dgram_out.hdr.ack_num = htons(expected_dgram);
	  
	  pthread_cond_signal(&data_available);
	  pthread_mutex_unlock(&lock);
	  
	  sendmsg_sim(sock_fd, dgram_out, &msgsend);
	  break;
	}
	
	// insertion in recv_wnd was successful (res=1)
	if(res!=0) print_recv_window(DBG_RECV_WNDW);
	
      }
      
      // send appropriate ACK and advertised window size
      bzero_dgram(&dgram_out);
      dgram_out.hdr.seq_num = htons(seq_num); seq_num++;
      dgram_out.hdr.window_size = htons(calculate_advertised_wnd_size());
      dgram_out.hdr.flag = (ACK);
      expected_dgram = calculate_expected_dgram();
      dgram_out.hdr.ack_num = htons(expected_dgram);
      
      pthread_cond_signal(&data_available);
      pthread_mutex_unlock(&lock);
      
      sendmsg_sim(sock_fd, dgram_out, &msgsend);
      
    }
  }
  
  // select to retransmit FIN-ACK and receive ACK, then end!
  int max_retx = 5;
  int count=0;
  
  FD_ZERO(&rset);
  for(;;){

    FD_SET(sock_fd, &rset);
    maxfd=sock_fd+1;
    
    res = select(maxfd, &rset, NULL, NULL, &timeout);
    if(res<0){
      if (errno==EINTR) continue;
      else {
      printf("select() error errno=%d\n", errno);exit(-1);}
    }

    // FIN-ACK timed out
    if(res==0){
      count++;
      if(count==max_retx){
	printf("Max retransmissions for FIN-ACK packet reached, closing procedure terminated\n\nFile Transfer completed, PRODUCER thread terminating\n");
	pthread_exit(NULL);
      }
	
      printf("FIN-ACK packet timed out, retransmitting (%d/%d)..\n", count, max_retx);
      dgram_out.hdr.seq_num = htons(seq_num); seq_num++;
      sendmsg_sim(sock_fd, dgram_out, &msgsend);
      timeout.tv_sec = CLIENT_TIMEOUT; timeout.tv_usec=0;
      continue;
    }
    
    if(FD_ISSET(sock_fd, &rset)){	
      
      if(recvmsg_sim(sock_fd, &dgram_in, &msgrecv)<0)continue;
      
      // check if packet is correct
      if( (dgram_in.hdr.flag != (ACK)) && (ntohs(dgram_in.hdr.seq_num) < expected_dgram)){
	printf("Invalid Packet received (ACK expected): "); print_packet_info(dgram_in); printf("\n"); continue;
      }

      // ACK received!

      break;
    }
  }

  printf("Closing mechanism ended, final ACK received\n\nFile Transfer completed, PRODUCER thread terminating\n");

  pthread_exit(NULL);
}


void *app_layer(){
  
  printf("CONSUMER thread started\n");
  
  double msec;
  int i,n, flag_FULL=0, flag_FIN=0, datalen=SEGMENT_SIZE-sizeof(header), datalen_last;
  char data_string[512];
  
  // consumer thread sends a duplicate ack if the window gets full
  datagram dup_ack; init_dgram(&dup_ack);
  struct msghdr msg;
  struct iovec iov[2];
  memset(&msg, 0, sizeof(struct msghdr));
  msg.msg_name = NULL;
  msg.msg_namelen = 0;
  msg.msg_iov = iov;
  msg.msg_iovlen = 2;
  iov[0].iov_base = (char*)(&(dup_ack.hdr));
  iov[0].iov_len = sizeof(header);
  iov[1].iov_base = dup_ack.payload;
  iov[1].iov_len = payloadlen;
  
  datagram *data_buff=(datagram*)malloc(input.recv_window_size*sizeof(datagram));
  for(i=0;i<input.recv_window_size;i++){
    init_dgram(&(data_buff[i]));
  }
  
  
#ifdef OUTPUT_TXT
  FILE* fp = fopen("output.txt", "w");
#endif
  
  while(1){
    
    msec = -1 * input.mu * log(drand48());
    usleep(msec*1000);
    //usleep(300*1000);
    
    // get lock, if empty, wait
    pthread_mutex_lock(&lock);
    if(calculate_advertised_wnd_size()==input.recv_window_size)
      pthread_cond_wait(&data_available,&lock);
    
    n = read_data(data_buff, &datalen_last, &flag_FULL, &flag_FIN);
    
    if(n>0) printf("\n\t------ %d datagram(s) with the following content read ------\n\n", n);
    
    for(i=0;i<n;i++){
      
      if( (i==(n-1)) && flag_FIN) datalen=datalen_last;
 
      
      fwrite(data_buff[i].payload,1,datalen,stdout); fflush(stdout);
#ifdef OUTPUT_TXT
      fwrite(data_buff[i].payload,1,datalen,fp);
#endif
      
    }
    
    if(n>0) printf("\n\n\t------ Finished printing %d datagram(s) content ------\n\n", n);
    
    if(flag_FIN){
      // release lock and signal
      pthread_cond_signal(&space_available);
      pthread_mutex_unlock(&lock);
      break;
    }
    
    if(flag_FULL!=0){
      bzero_dgram(&dup_ack);
      dup_ack.hdr.seq_num = htons(seq_num); seq_num++;
      dup_ack.hdr.window_size = htons(calculate_advertised_wnd_size());
      dup_ack.hdr.ack_num = htons(calculate_expected_dgram());
      dup_ack.hdr.flag = (ACK);
  
      sendmsg_sim(sock_fd, dup_ack, &msg);
      
      printf("The receiving window has been emptied. To avoid deadlock, ACK%d with new window size %d has been sent to the server (window update)\n", calculate_expected_dgram(), calculate_advertised_wnd_size());
      flag_FULL=0;
    }
    
    // release lock and signal
    pthread_cond_signal(&space_available);
    pthread_mutex_unlock(&lock);
    
  }
  
#ifdef OUTPUT_TXT
  fclose(fp);
#endif
  
  printf("\n\nAll of the datagrams have been received and printed\nCONSUMER thread terminating\n");
    
  pthread_exit(NULL);
}

