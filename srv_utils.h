#include "unpifiplus.h"
#include "unprtt_int.h"
#include <sys/time.h>
#include <math.h>

#define MAX_LENGTH 255
#define SEGMENT_SIZE 512
// persistent timer range (in ms)
#define MIN_PERSISTENT_TIMER 5000
#define MAX_PERSISTENT_TIMER 60000

typedef struct{
  int port;
  int send_window_size;
}serv_input;


typedef struct{		
  int skfd;
  struct sockaddr_in server_ip;
  struct sockaddr_in netmsk;
  struct sockaddr_in subntadd;
}serv_strct;

typedef enum {SYN=1, ACK=2, FIN=4, PROBE=8} flag_t;

typedef struct{
  int seq_num;
  int ack_num;
  flag_t flag;
  int window_size;
}header;

typedef struct{
  header hdr;
  void* payload; // dynamically allocated SEGMENT_SIZE - sizeof(struct header)
}datagram;

typedef struct{
        pid_t pid;
        struct sockaddr_in client_ip;
        int client_port;
} client_info;

// client info item
typedef struct client_element{
	client_info cli_info;
	struct client_element* next;
}client_item;
//typedef struct  client_item;
typedef client_item* client_list;


// access function to client_info lists
client_list insert(client_list l, client_info cli);  // inserts a client_info structure into the list
int contains(client_list l, struct sockaddr_in client_ip, int client_port);  // returns 1 if the list contains an entry with same client ip and port
client_list rmv(client_list l, pid_t pid);  // removes the client_info with that particular pid from the list and returns the list without that entry
void print_client_list(client_list l);

// reads from the server input file
void serv_read_input(char* filename, serv_input* in);
// initializes a datagram: sets all fields to zero and allocates memory for payload (header+payload must be 512 bytes)
int init_dgram(datagram* dgram);

void bzero_dgram(datagram* dgram);

void print_packet_info(datagram dgram_out);

void init_msgdr(struct msghdr* msg, struct iovec* iov, datagram* dgram, int payloadlen);

int get_dgram_seqnum(datagram dgram);

uint32_t get_ts();

int get_dgram_acknum(datagram dgram);

void init_send_window(int size, int adv_wnd_size);

void insert_send_window(datagram dgram_in, int payloadlen, struct rtt_info *rttinfo);

void clear_send_window(int ackno);

int is_cumulative_ack(int ackno);

void print_send_window(int max);

int send_wnd_size();

int max_send_wnd_size();
// returns -1 if EOF reached
int build_data_packet(datagram* dgram_out, FILE* fp);

int get_last_ack();

void get_oldest_pending_packet(datagram *dest);

int inc_count_dup_acks();

void set_retransmitted();

int is_duplicate_ack(int ackno);

int is_retransmitted(int ackno);

void set_recv_wnd_size(int val);

void set_cwnd(int val);
int get_cwnd();

void set_ssthreshold(int val);
int get_ssthreshold();

void set_itimeval(struct itimerval* t, long sec, long usec);

// persistent timer functions
void init_persistent_timer(uint32_t* timeval);
void minmax_persistent_timer(uint32_t* timerval);
void double_persistent_timer(uint32_t* timeval);





