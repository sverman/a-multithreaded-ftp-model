// Configuration file (see item 8 of ReadMe file)

// maximum number of slots in the SERVER's sending window that are monitored/printed on stdout during runtime
#define DBG_SEND_WNDW	20

// maximum number of slots in the CLIENT's receiving window that are monitored/printed on stdout during runtime
#define DBG_RECV_WNDW	20

// Comment this line to disable the consumer's thread output on file, uncomment to enable
#define OUTPUT_TXT

