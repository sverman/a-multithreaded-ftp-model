# Marco Rondinini 108691382
# Sambhav Verman 108768949

CC = gcc

LIBS = -lresolv -lnsl -lm -lpthread /home/courses/cse533/Stevens/unpv13e_solaris2.10/libunp.a\

FLAGS = -g -O2

CFLAGS = ${FLAGS} -I/home/courses/cse533/Stevens/unpv13e_solaris2.10/lib -I/home/courses/cse533/Asgn2_code/


all: client server

client: client.o get_ifi_info_plus.o cli_utils.o
	${CC} ${CFLAGS} -o client client.o get_ifi_info_plus.o cli_utils.o ${LIBS}
client.o: client.c
	${CC} ${CFLAGS} -c client.c
get_ifi_info_plus.o: /home/courses/cse533/Asgn2_code/get_ifi_info_plus.c
	${CC} ${CFLAGS} -c /home/courses/cse533/Asgn2_code/get_ifi_info_plus.c
cli_utils.o: cli_utils.c
	${CC} ${CFLAGS} -c cli_utils.c


server: server.o srv_utils.o rtt_int.o get_ifi_info_plus.o
	${CC} ${CFLAGS} -o server server.o get_ifi_info_plus.o srv_utils.o rtt_int.o ${LIBS}
server.o: server.c
	${CC} ${CFLAGS} -c server.c
srv_utils.o: srv_utils.c
	${CC} ${CFLAGS} -c srv_utils.c
rtt_int.o: rtt_int.c
	${CC} ${CFLAGS} -c rtt_int.c
	
	

clean:
	rm client client.o get_ifi_info_plus.o cli_utils.o srv_utils.o rtt_int.o server server.o




