#include "cli_utils.h"
#include "unpifiplus.h"
#include <sys/time.h>
#include <errno.h>


void print_ifi_info_plus(struct ifi_info *temp){

  struct ifi_info *ifi=temp;
  u_char *ptr;
  int i;
  struct sockaddr *sa;
  
  
  for(; ifi!=NULL; ifi=ifi->ifi_next){

    		printf("%s: ", ifi->ifi_name);
		if (ifi->ifi_index != 0)
			printf("(%d) ", ifi->ifi_index);
		printf("<");
/* *INDENT-OFF* */
		if (ifi->ifi_flags & IFF_UP)			printf("UP ");
		if (ifi->ifi_flags & IFF_BROADCAST)		printf("BCAST ");
		if (ifi->ifi_flags & IFF_MULTICAST)		printf("MCAST ");
		if (ifi->ifi_flags & IFF_LOOPBACK)		printf("LOOP ");
		if (ifi->ifi_flags & IFF_POINTOPOINT)	printf("P2P ");
		printf(">\n");
/* *INDENT-ON* */

		if ( (i = ifi->ifi_hlen) > 0) {
			ptr = ifi->ifi_haddr;
			do {
				printf("%s%x", (i == ifi->ifi_hlen) ? "  " : ":", *ptr++);
			} while (--i > 0);
			printf("\n");
		}
		if (ifi->ifi_mtu != 0)
			printf("  MTU: %d\n", ifi->ifi_mtu);

		if ( (sa = ifi->ifi_addr) != NULL)
			printf("  IP addr: %s\n",
						Sock_ntop_host(sa, sizeof(*sa)));

/*=================== cse 533 Assignment 2 modifications ======================*/

		if ( (sa = ifi->ifi_ntmaddr) != NULL)
			printf("  network mask: %s\n",
						Sock_ntop_host(sa, sizeof(*sa)));

/*=============================================================================*/

		if ( (sa = ifi->ifi_brdaddr) != NULL)
			printf("  broadcast addr: %s\n",
						Sock_ntop_host(sa, sizeof(*sa)));
		if ( (sa = ifi->ifi_dstaddr) != NULL)
			printf("  destination addr: %s\n",
						Sock_ntop_host(sa, sizeof(*sa)));
	}
}

void select_appropriate_ip_addresses(struct ifi_info* ifi, struct sockaddr_in* cli_ip, struct sockaddr_in* serv_ip, char* server_ip_str, int* flags){

  
  struct ifi_info *ifiptr;
  struct sockaddr_in* client_ip = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
  char client_ip_str[16];
  struct sockaddr_in* client_nm = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
  struct sockaddr_in* server_ip = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
  struct in_addr subnet_cli, subnet_serv;
  char subnet_str[16];
  // longest prefix match in case of multiple entry matches
  struct sockaddr_in sa_zero; memset(&sa_zero, 0, sizeof(sa_zero));
  struct sockaddr_in* client_ip_lpm = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
  struct sockaddr_in* client_nm_lpm = (struct sockaddr_in*)malloc(sizeof(struct sockaddr_in));
  memset(client_ip_lpm, 0, sizeof(struct sockaddr_in));
  memset(client_nm_lpm, 0, sizeof(struct sockaddr_in));
  
  // convert server ip address to binary form
  if(inet_pton(AF_INET, server_ip_str, &(server_ip->sin_addr))<=0){
    
    printf("Invalid Server Ip address\n");
    exit(-1);
    
  }
  
  // Determine if the server is on the same host or local
  ifiptr=ifi;
  while(ifiptr!=NULL){
    
       if ( ((client_ip = (struct sockaddr_in*)ifiptr->ifi_addr) == NULL) || ((client_nm = (struct sockaddr_in*)ifiptr->ifi_ntmaddr) == NULL)){
	 printf("Error reading ifi_info\n");
	 ifiptr=ifiptr->ifi_next;
	 continue;
       }

       // is it on the same host?
      if(memcmp(&client_ip->sin_addr, &server_ip->sin_addr, sizeof(struct in_addr))==0){
	
	if(inet_pton(AF_INET, "127.0.0.1", &(server_ip->sin_addr))<=0){printf("Cannot assign loopback address to Server IP\n");exit(-1);}
	if(inet_pton(AF_INET, "127.0.0.1", &(client_ip->sin_addr))<=0){printf("Cannot assign loopback address to Client IP\n");exit(-1);}
	printf("The server is located on the same host\n");
	memset(client_ip_lpm, 0, sizeof(struct sockaddr_in));
	
	// SET THE MSG OPTION TO DONT ROUTE
	*flags = SO_DONTROUTE;
	
	break;
      }
      
      // I obtain the two subnet addresses and compare them
      subnet_cli.s_addr = client_ip->sin_addr.s_addr & client_nm->sin_addr.s_addr;
      subnet_serv.s_addr = server_ip->sin_addr.s_addr & client_nm->sin_addr.s_addr;
      if( (memcmp(&subnet_cli, &subnet_serv, sizeof(struct in_addr))==0) && (client_nm->sin_addr.s_addr >= client_nm_lpm->sin_addr.s_addr) ){
	// update longest prefix match candidate
	memcpy(client_ip_lpm, client_ip, sizeof(struct sockaddr_in));
	memcpy(client_nm_lpm, client_nm, sizeof(struct sockaddr_in));
	
	if(inet_ntop(AF_INET, &subnet_cli, subnet_str, sizeof(subnet_str))<=0){printf("Cannot convert Subnet address to readable form\n");exit(-1);}
	if(inet_ntop(AF_INET, &(client_ip_lpm->sin_addr), client_ip_str, sizeof(client_ip_str))<=0){printf("Cannot convert Client IP address to readable form\n");exit(-1);}
	printf("The server (%s) is local to subnet address: %s for IP: %s\n", server_ip_str, subnet_str, client_ip_str);
	
	// SET THE MSG OPTION TO DONT ROUTE
	*flags = SO_DONTROUTE;
	
      }
      
      ifiptr=ifiptr->ifi_next;
  }
  if( (ifiptr==NULL) && (memcmp(&(client_ip_lpm->sin_addr), &sa_zero, sizeof(struct in_addr))==0)){
    // host is not local
    printf("The server is not local. Client IP address chosen arbitrarily\n");
    // I decide to choose the last IP address in the ifi_info list
  }

  // in case the server is local, we select the longest prefix match as our IP address
  if(memcmp(&(client_ip_lpm->sin_addr), &sa_zero, sizeof(struct in_addr))!=0)
    memcpy(client_ip, client_ip_lpm, sizeof(struct sockaddr_in));
 
  
  memcpy(cli_ip, client_ip, sizeof(struct sockaddr_in));
  memcpy(serv_ip, server_ip, sizeof(struct sockaddr_in));
    
  // free
  free(client_ip_lpm); free(client_nm_lpm); free(client_nm); free(client_ip); free(server_ip);
     
  return;
}

void cli_read_input(char* filename, cli_input* in){
  
  FILE* fp = fopen(filename, "r");
  if(fp==NULL){
    printf("Error opening input file %s\n", filename);
    exit(-1);
  }
  
    char server_ip_str[16];
  int serv_port;
  char file_name[MAX_LENGTH];
  int recv_window_size;
  int seed;
  float p;
  int mu;
  
  int n=0;
  if((n=fscanf(fp, "%s%d\n%s%d\n%d%f%d", in->server_ip_str, &(in->serv_port), in->file_name, &(in->recv_window_size), &(in->seed), &(in->p), &(in->mu)))!=7){
    printf("Error reading inputs from %s (%d out of 7 read)\n", filename, n);
    exit(-1);
  }
  fclose(fp);
  
  // DBG
  printf("  IP address of the server: %s\n", in->server_ip_str);
  printf("  Well-known port number of server: %d\n", in->serv_port);
  printf("  Filename to be transferred: %s\n", in->file_name);
  printf("  Receiving sliding window size: %d\n", in->recv_window_size);
  printf("  Random generator seed value: %d\n", in->seed);
  printf("  Probability p of datagram loss (between 0.0 and 1.0): %1.2f\n", in->p);
  printf("  The mean µ, in milliseconds: %d\n", in->mu);
  
  return;
}

// operations on datagrams
int init_dgram(datagram* dgram){
  memset((struct header*)(&(dgram->hdr)), 0, sizeof(header));
  dgram->payload = (void*)malloc(SEGMENT_SIZE - sizeof(header));
  memset((void*)(dgram->payload), 0, SEGMENT_SIZE-sizeof(header));
  return (SEGMENT_SIZE-sizeof(header));
}

void bzero_dgram(datagram* dgram){
  
  memset((struct header*)(&(dgram->hdr)), 0, sizeof(header));
  memset((void*)(dgram->payload), 0, SEGMENT_SIZE-sizeof(header));
}

void print_packet_info(datagram dgram_out){
  printf("SeqNo=%u, AckNo=%u, Flag=", ntohs(dgram_out.hdr.seq_num), ntohs(dgram_out.hdr.ack_num));
  if(dgram_out.hdr.flag == (0)) printf("N/A ");
  if((dgram_out.hdr.flag & SYN) == (SYN)) printf("SYN-");
  if((dgram_out.hdr.flag & FIN) == (FIN)) printf("FIN-");
  if((dgram_out.hdr.flag & ACK) == (ACK)) printf("ACK-");
  if((dgram_out.hdr.flag & PROBE) == (PROBE)) printf("PROBE-");
  printf("\b, WindowSize=%d", htons(dgram_out.hdr.window_size));
  
}

int get_dgram_seqnum(datagram dgram){
  return ntohs(dgram.hdr.seq_num);
}

// send and receive that compute the dropping probability
float loss_prob;
void sendmsg_sim(int sockfd, datagram dgram, struct msghdr* msg){
  
    float rnd = drand48();
    if (rnd < loss_prob){
      printf("Packet: "); print_packet_info(dgram); printf(" (outgoing) DROPPED (%1.2f<%1.2f)\n", rnd, loss_prob);
      return;
    }

    if(sendmsg(sockfd, msg, 0)<0){
      printf("sendmsg() error for packet %d - Is the peer still connected?\n", get_dgram_seqnum(dgram));
    }
    else{
      printf("Packet: "); print_packet_info(dgram); printf(" SENT\n");
    }
}

int recvmsg_sim(int sockfd, datagram* dgram, struct msghdr* msg){
  
  
     if(recvmsg(sockfd, msg, 0)<0) return -1;

    float rnd = drand48();
    if (rnd < loss_prob){
      printf("Packet: "); print_packet_info(*dgram); printf(" (incoming) DROPPED (%1.2f<%1.2f)\n", rnd, loss_prob);
      return -1;
    }
    
    printf("Packet: "); print_packet_info(*dgram); printf(" RECEIVED\n");
    return 1;
}

void init_dropper_sim(int seed, float p){
  srand48(seed);
  loss_prob = p;
}

// definition of sliding window and setter/getter functions
datagram* recv_window = NULL;
int start=0, max_size=0, advertised_wnd_size=0, last_dgram_consumed=0; // mod by consumer thread modifies last_dgram_consumed and start variables

void destroy_recv_window(){
  free(recv_window);
}

void init_recv_window(int size, datagram* d){
  max_size = size;
  advertised_wnd_size = size;

  recv_window = d;
}

int wnd_abs_index(int base, int rel_index){
  
  return ( (base+rel_index) % max_size );
}

int calculate_expected_dgram(){
  
  int i=0;
  while( (get_dgram_seqnum(recv_window[wnd_abs_index(start,i)]) != 0) ){i++; if(wnd_abs_index(start,i)==start) return last_dgram_consumed+1+max_size;}
  
  // i == delta index that contains the first missing element

  return last_dgram_consumed+1+i;
  
}

int calculate_advertised_wnd_size(){
  int i=start;
  while( (get_dgram_seqnum(recv_window[i]) != 0) ){i=wnd_abs_index(i,1); if(i==start) return 0;}
  
  // i == abs index that contains the first missing element
  if(i==start) return max_size;
  
  int count=0;
  while(i != start){
     count++;
//     // increment i until we consume all the window
    i=wnd_abs_index(i,1);
  }
  return count;
}

void dgram_cpy(datagram* dest, datagram* src, int payloadlen){
  
  memcpy(&(dest->hdr), &(src->hdr), sizeof(header));
  memcpy(dest->payload, src->payload, payloadlen);
}

void insert_dgram(datagram dgram_in, int payloadlen){
  
  int rel_index = (get_dgram_seqnum(dgram_in) - (last_dgram_consumed+1));
  int abs_index = wnd_abs_index(start, rel_index);
  
  // if it's already there, I don't insert it
  if( get_dgram_seqnum(recv_window[abs_index]) == get_dgram_seqnum(dgram_in) ){
   printf("Received copy of Datagram (%d). Insertion into Receiving window not necessary\n", get_dgram_seqnum(recv_window[abs_index]));
   return; 
  }
  
  dgram_cpy(&(recv_window[abs_index]), &dgram_in, payloadlen);
  
  printf("Datagram (SeqNo=%d) inserted into receiving window slot %d\n", get_dgram_seqnum(dgram_in), rel_index);
  
  // modify advertised_wnd_size
  if( (advertised_wnd_size = calculate_advertised_wnd_size()) == 0)
    printf("Receiving window full, waiting for consumer to read its data\n");
 
}

void print_recv_window(int max){
  int n, i=0;
  if(max<max_size) n=max;
  else n=max_size;
  printf("Recv Window status (first %d elements): ",n);
  
  do{
    
    int seqno = get_dgram_seqnum(recv_window[wnd_abs_index(start,i)]);
    
    if(seqno==0) printf("[ ]");
    else printf("[%d]", seqno);
    
    i++;
  }while( i<n ); //{i++; if(wnd_abs_index(i+start)==start) return last_dgram_consumed+1+max_size;}
  
  printf(" adv_wnd_size=%d\n",advertised_wnd_size);
}

void dbg_mod_start(int index){
  start = index;
}

void dbg_mod_last_dgram_consumed(int seq_no){
  last_dgram_consumed = seq_no;
}


void init_msgdr(struct msghdr* msg, struct iovec* iov, datagram* dgram, int payloadlen){
  
  memset(msg, 0, sizeof(struct msghdr));
  //memset(&msgrecv, 0, sizeof(struct msghdr));
  
  msg->msg_name = NULL;
  msg->msg_namelen = 0;
  msg->msg_iov = iov;
  msg->msg_iovlen = 2;
  iov[0].iov_base = (char*)(&(dgram->hdr));
  iov[0].iov_len = sizeof(header);
  iov[1].iov_base = dgram->payload;
  iov[1].iov_len = payloadlen;
}

int fin_dgram = -1;
int process_packet(datagram dgram_in){
  
  // packet discarded (like TCP) if it doesn't fit the receiving buffer
  if(get_dgram_seqnum(dgram_in) >  last_dgram_consumed+max_size){
    printf("Invalid (out of valid range) Packet received: SeqNo=%d\n",get_dgram_seqnum(dgram_in));
    return 0;
  }
  
  int payloadlen = (SEGMENT_SIZE-sizeof(header));

  if(dgram_in.hdr.flag == (FIN)){
    printf("Producer: last packet (SeqNo=%d) received (%d Data Bytes)\n", ntohs(dgram_in.hdr.seq_num), ntohs(dgram_in.hdr.window_size));
    payloadlen = ntohs(dgram_in.hdr.window_size);
    fin_dgram = get_dgram_seqnum(dgram_in);
  }

  // insert datagram into recv window
  insert_dgram(dgram_in, payloadlen);
  
  // if all packets up to the last one have been received and are ready to be consumed by the consumer thread, return -1, else return 1
  if( (calculate_expected_dgram()-1) == fin_dgram )
    return -1;
  return 1;
}

// returns the number of datagrams written into dgram_buff -- dgram_buff must be initialized, and should be big enough to be able to accomodate max_size datagrams
int read_data(datagram* dgram_buff, int* datalen, int* flag_FULL, int* flag_FIN){
  
  // is there data?
  if(get_dgram_seqnum(recv_window[start])==0) return 0;
  
  // if it's full, upon consuming the buffer, the client should notify the server with a duplicate ACK letting him know the expected datagram and new window size
  if(advertised_wnd_size==0) *flag_FULL=1;
  
  int i=0, payloadlen = (SEGMENT_SIZE-sizeof(header));
  do{
    
    // if FIN received, I only read the necessary bytes and return -1
    if(recv_window[start].hdr.flag == (FIN)){
      
      payloadlen = ntohs(recv_window[start].hdr.window_size);
      *datalen = payloadlen;
      *flag_FIN=1;
    }
    
    dgram_cpy(&(dgram_buff[i]), &(recv_window[start]), payloadlen);
    i++;
    
    last_dgram_consumed = get_dgram_seqnum(recv_window[start]);
    
    bzero_dgram(&(recv_window[start]));
    
  }while(get_dgram_seqnum(recv_window[(start=wnd_abs_index(start,1))])!=0);
  
  return i;
}
