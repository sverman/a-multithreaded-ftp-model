#include "unpifiplus.h"
#include "unprtt_int.h"

#define MAX_LENGTH 255
#define SEGMENT_SIZE 512

typedef struct{
  char server_ip_str[16];
  int serv_port;
  char file_name[MAX_LENGTH];
  int recv_window_size;
  int seed;
  float p;
  int mu;
}cli_input;

// type of flags within a packet
typedef enum {SYN=1, ACK=2, FIN=4, PROBE=8} flag_t;

typedef struct{
  int seq_num;
  int ack_num;
  flag_t flag;
  int window_size;
}header;

typedef struct{
  header hdr;
  void* payload; // dynamically allocated SEGMENT_SIZE - sizeof(struct header)
}datagram;

// reads from the client input file
void cli_read_input(char* filename, cli_input* in);
// prints the list of ip addresses and associated network masks
void print_ifi_info_plus(struct ifi_info *temp);
// selects an appropriate IP address for the client and server depending on the location of the server (same host, same network or remote location) and sets the MSG_DONTROUTE when possible
void select_appropriate_ip_addresses(struct ifi_info* ifi, struct sockaddr_in* cli_ip, struct sockaddr_in* serv_ip, char* server_ip_str, int* flags);
// initializes a datagram: sets all fields to zero and allocates memory for payload (header+payload must be 512 bytes)
int init_dgram(datagram* dgram);

void bzero_dgram(datagram* dgram);

void print_packet_info(datagram dgram_out);

// initializes the random number generator with the seed read from the file client.in
void init_dropper_sim(int seed, float p);

void init_msgdr(struct msghdr* msg, struct iovec* iov, datagram* dgram, int payloadlen);

void destroy_recv_window();

void init_recv_window(int size, datagram* d);

void insert_dgram(datagram dgram_in, int payloadlen);
// the consumer thread calls this function to read as many packet as possible from the reciving window. If flag_FULL is set, the consumer thread should notify the server with the new expected_dgram and window_size
int read_data(datagram* dgram_buff, int* datalen, int* flag_FULL, int* flag_FIN);

int calculate_advertised_wnd_size();

int calculate_expected_dgram(); 
// prints the first datagrams in the receiving window (up to max)
void print_recv_window(int max);

// DEBUG functions
void dbg_mod_start(int index);
void dbg_mod_last_dgram_consumed(int seq_no);

int get_dgram_seqnum(datagram dgram);

void sendmsg_sim(int sockfd, datagram dgram, struct msghdr* msg);

int recvmsg_sim(int sockfd, datagram* dgram, struct msghdr* msg);

int process_packet(datagram dgram_in);
